package ir.code.challenge.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import ir.code.challenge.R
import ir.code.challenge.ui.intro.IntroActivity
import ir.code.challenge.ui.intro.viewholder.MovieViewHolder
import ir.code.challenge.utiles.EspressoIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MovieFragmentListTest {
    val FIRST_MOVIE_TITLE = "The Batman"

    @get:Rule
    val activityRule = ActivityScenarioRule(IntroActivity::class.java)

    @Before
    fun registeringResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unregisteringResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    @Test
    fun selected_movie_test() {
        onView(withId(R.id.movie_list)).perform(actionOnItemAtPosition<MovieViewHolder>(0, click()))
        onView(withId(R.id.title)).check(matches(withText(FIRST_MOVIE_TITLE)))

        pressBack()
    }

    @Test
    fun api_companies_test() {
        onView(withId(R.id.movie_list)).perform(actionOnItemAtPosition<MovieViewHolder>(4, click()))
        onView(withId(R.id.companies)).check(matches(isDisplayed()))
    }
}