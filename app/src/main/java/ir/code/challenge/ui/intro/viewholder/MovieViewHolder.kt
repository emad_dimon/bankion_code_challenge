package ir.code.challenge.ui.intro.viewholder

import ir.code.challenge.BuildConfig
import ir.code.challenge.data.network.model.Movie
import ir.code.challenge.databinding.ItemMovieBinding
import ir.code.challenge.utiles.ax.Ax
import ir.code.challenge.utiles.base.view.BaseViewHolder

class MovieViewHolder(var binding: ItemMovieBinding): BaseViewHolder<Movie>(binding) {

    override fun bind(item: Movie) {
        binding.poster.clipToOutline = true
        binding.date.text = item.release
        binding.title.text = item.title

        Ax.load(BuildConfig.IMAGE_URL +  item.poster).into(binding.poster)
    }
}