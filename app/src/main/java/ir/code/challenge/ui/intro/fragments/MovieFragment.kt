package ir.code.challenge.ui.intro.fragments

import android.os.Bundle
import android.view.LayoutInflater
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.code.challenge.ui.intro.IntroActivity
import ir.code.challenge.ui.intro.adapters.MovieListAdapter
import ir.code.challenge.ui.intro.viewmodel.MovieViewModel
import ir.code.challenge.ui.intro.viewmodel.MovieViewModelFactory
import ir.code.challenge.R
import ir.code.challenge.databinding.FragmentMovieBinding
import ir.code.challenge.utiles.EspressoIdlingResource
import ir.code.challenge.utiles.InjectorUtils
import ir.code.challenge.utiles.base.view.BaseFragment

class MovieFragment : BaseFragment<FragmentMovieBinding, MovieViewModel>() {

    private val adapter = MovieListAdapter()

    private fun observer() {
        viewModel.moviesLiveData.observe(this) {
            EspressoIdlingResource.decrement()

            if (adapter.page == it.page) {
                adapter.loading(false)

                if (adapter.totalPages == null)
                    adapter.totalPages = it.totalPages

                adapter.items = it.results
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observer()
    }

    override fun bind(inflater: LayoutInflater?): FragmentMovieBinding =
        FragmentMovieBinding.inflate(inflater!!)

    override fun initView() {
        activity?.setActionBar(binding.toolbar)
        val layoutManager = GridLayoutManager(mContext, 2, RecyclerView.VERTICAL, false)
        binding.movieList.adapter = adapter.also {
            it.recyclerView = binding.movieList
        }
        binding.movieList.layoutManager = layoutManager
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (adapter.getItemViewType(position)) {
                    0 -> 2
                    1 -> 1
                    else -> 1
                }
            }
        }


        adapter.clickListener = { poster, position ->
            val item = adapter.items?.get(position)

            val fragmentExtras = FragmentNavigatorExtras(
                poster to "poster"
            )

            viewModel.setSelectedMovie(item)

            val navController: NavController =
                Navigation.findNavController(activity as IntroActivity, R.id.nav_host_fragment)
            navController.navigate(
                MovieFragmentDirections.actionNavMovieToNavMovieDetail(item?.id ?: -1)
            )
        }
        adapter.touchBottom(binding.movieList) { _, to ->
            if (to < adapter.totalPages ?: Int.MAX_VALUE) {
                adapter.loading(true)
                getMovie(to)
            }
        }

        getMovie()
    }

    private fun getMovie(page: Int = 1) {
        viewModel.getMoviesList(page)
        EspressoIdlingResource.increment()
    }

    override fun createViewModel(): MovieViewModel {
        val tableViewModelFactory = MovieViewModelFactory(this, InjectorUtils.appClient(mContext))
        val vm: MovieViewModel by navGraphViewModels(R.id.main_graph) { tableViewModelFactory }
        return vm
    }
}