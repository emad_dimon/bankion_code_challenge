package ir.code.challenge.ui.intro.fragments

import android.view.LayoutInflater
import androidx.navigation.navGraphViewModels
import androidx.transition.ChangeBounds
import ir.code.challenge.ui.intro.viewmodel.MovieViewModel
import ir.code.challenge.ui.intro.viewmodel.MovieViewModelFactory
import ir.code.challenge.BuildConfig
import ir.code.challenge.R
import ir.code.challenge.data.network.model.Movie
import ir.code.challenge.databinding.FragmentMovieDetailBinding
import ir.code.challenge.utiles.EspressoIdlingResource
import ir.code.challenge.utiles.InjectorUtils
import ir.code.challenge.utiles.ax.Ax
import ir.code.challenge.utiles.base.view.BaseFragment

class MovieDetailFragment : BaseFragment<FragmentMovieDetailBinding, MovieViewModel>() {

    private fun observer() {
        viewModel.movieDetailLiveData.observe(this) {
            updatePage(it)
            EspressoIdlingResource.decrement()
        }
        viewModel.selectedMovieLiveData.observe(this) {
            updatePage(it)
        }
    }

    private fun updatePage(movie: Movie) {
        Ax.load(BuildConfig.IMAGE_URL + movie.backdrop).into(binding.backdropImage)
        Ax.load(BuildConfig.IMAGE_URL + movie.poster).into(binding.poster.apply {
            clipToOutline = true
        })

        binding.title.text = movie.title
        binding.description.text = movie.overview
        binding.votte.text = movie.vote.toString()
        binding.companies.text = movie.companies?.joinToString("            ") {  it.name }
    }

    override fun bind(inflater: LayoutInflater?): FragmentMovieDetailBinding =
        FragmentMovieDetailBinding.inflate(inflater!!)

    override fun initView() {
        observer()

        arguments.let {
            if (it != null) {
                val movieId = MovieDetailFragmentArgs.fromBundle(it).movieId
                if (movieId != -1) {
                    viewModel.getDetail(movieId)
                    EspressoIdlingResource.increment()
                }
            }
        }

        sharedElementEnterTransition = ChangeBounds().apply {
            duration = 750
        }
        sharedElementReturnTransition = ChangeBounds().apply {
            duration = 750
        }
    }

    override fun createViewModel(): MovieViewModel {
        val tableViewModelFactory = MovieViewModelFactory(this, InjectorUtils.appClient(mContext))
        val vm: MovieViewModel by navGraphViewModels(R.id.main_graph) { tableViewModelFactory }
        return vm
    }
}