package ir.code.challenge.ui.intro

import android.view.LayoutInflater
import androidx.navigation.fragment.NavHostFragment
import ir.code.challenge.R
import ir.code.challenge.databinding.ActivityMainBinding
import ir.code.challenge.utiles.base.view.BaseActivity

class IntroActivity : BaseActivity<ActivityMainBinding>() {

    override fun bind(): ActivityMainBinding = ActivityMainBinding.inflate(LayoutInflater.from(this))

    override fun initView() {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
    }
}