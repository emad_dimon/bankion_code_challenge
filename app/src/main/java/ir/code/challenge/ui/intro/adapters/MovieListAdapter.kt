package ir.code.challenge.ui.intro.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ir.code.challenge.data.network.model.Movie
import ir.code.challenge.databinding.ItemLoadingBinding
import ir.code.challenge.databinding.ItemMovieBinding
import ir.code.challenge.ui.intro.viewholder.LoadingViewHolder
import ir.code.challenge.ui.intro.viewholder.MovieViewHolder
import ir.code.challenge.utiles.base.view.BaseAdapter
import ir.code.challenge.utiles.base.view.BaseViewHolder

class MovieListAdapter: BaseAdapter<Movie, BaseViewHolder<Movie>>() {
    var clickListener: ((View, Int) -> Unit)? = null

    override fun inflate(parent: ViewGroup, viewType: Int): BaseViewHolder<Movie> {
        val viewHolder: BaseViewHolder<Movie> = if(viewType == 1) {
            val itemBinding =
                ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            itemBinding.root.setOnClickListener { view ->
                clickListener?.invoke(
                    itemBinding.poster,
                    recyclerView?.getChildLayoutPosition(view) ?: -1
                )
            }
            MovieViewHolder(itemBinding)
        } else {
            val itemBinding =
                ItemLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            LoadingViewHolder(itemBinding)
        }

        return viewHolder
    }
}