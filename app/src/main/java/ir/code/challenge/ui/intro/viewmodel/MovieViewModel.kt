package ir.code.challenge.ui.intro.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import ir.code.challenge.utiles.base.BaseModel
import ir.code.challenge.data.network.AppClient
import ir.code.challenge.data.network.model.Movie
import ir.code.challenge.data.network.services.MovieService
import ir.code.challenge.utiles.LifeCycleAwarePushReceiver
import ir.code.challenge.utiles.base.view.BaseViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieViewModel(lifecycleRegistry: LifecycleOwner, private val appClient: AppClient) : BaseViewModel() {

    init {
        LifeCycleAwarePushReceiver(lifecycleRegistry) {
            appClient.client?.dispatcher?.cancelAll()
        }
    }

    private var _movieDetailLiveData: MutableLiveData<Movie> = MutableLiveData()
    private var _moviesLiveData: MutableLiveData<BaseModel<Movie>> = MutableLiveData()
    private var _movieLiveData: MutableLiveData<Movie> = MutableLiveData()

    val moviesLiveData: MutableLiveData<BaseModel<Movie>>
        get() {
            return _moviesLiveData
        }

    val selectedMovieLiveData: MutableLiveData<Movie>
        get() {
            return _movieLiveData
        }

    val movieDetailLiveData: MutableLiveData<Movie>
    get() {
        return _movieDetailLiveData
    }

    fun setSelectedMovie(item: Movie?) {
        if (item != null)
            this._movieLiveData.value = item
    }

    fun getDetail(id: Int) {
        appClient
            .getService(MovieService::class.java)
            .detail(id)
            .enqueue(object : Callback<Movie> {
                override fun onResponse(
                    call: Call<Movie>,
                    response: Response<Movie>
                ) {
                    val body = response.body()
                    if (body != null && response.isSuccessful) {
                        movieDetailLiveData.value = body
                    }
                }

                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    if(!call.isCanceled)
                        errorLiveData.value = t.message
                }
            })
    }

    fun getMoviesList(page: Int = 1) {
        appClient
            .getService(MovieService::class.java)
            .popular(page)
            .enqueue(object : Callback<BaseModel<Movie>> {
                override fun onResponse(
                    call: Call<BaseModel<Movie>>,
                    response: Response<BaseModel<Movie>>
                ) {
                    val body = response.body()
                    if (body != null && response.isSuccessful) {
                        moviesLiveData.value = body
                    }
                }

                override fun onFailure(call: Call<BaseModel<Movie>>, t: Throwable) {
                    if(!call.isCanceled)
                        errorLiveData.value = t.message
                }
            })
    }
}