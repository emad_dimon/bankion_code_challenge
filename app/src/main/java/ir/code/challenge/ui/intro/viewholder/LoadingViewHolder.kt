package ir.code.challenge.ui.intro.viewholder
import ir.code.challenge.data.network.model.Movie
import ir.code.challenge.databinding.ItemLoadingBinding
import ir.code.challenge.utiles.base.view.BaseViewHolder

class LoadingViewHolder(binding: ItemLoadingBinding): BaseViewHolder<Movie>(binding) {
    override fun bind(item: Movie) {
    }
}