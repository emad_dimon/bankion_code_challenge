package ir.code.challenge.ui.intro.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.code.challenge.data.network.AppClient

class MovieViewModelFactory(val lifecycleOwner: LifecycleOwner, val appClient: AppClient): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(LifecycleOwner::class.java, AppClient::class.java).newInstance(lifecycleOwner, appClient)
    }
}