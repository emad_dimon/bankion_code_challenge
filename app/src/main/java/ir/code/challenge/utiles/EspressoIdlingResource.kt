package ir.code.challenge.utiles

import androidx.test.espresso.idling.CountingIdlingResource


object EspressoIdlingResource {
    @JvmField val countingIdlingResource = CountingIdlingResource("test")

    fun increment() {
        countingIdlingResource.increment()
    }

    fun decrement() {
        if(!countingIdlingResource.isIdleNow)
            countingIdlingResource.decrement()
    }
}