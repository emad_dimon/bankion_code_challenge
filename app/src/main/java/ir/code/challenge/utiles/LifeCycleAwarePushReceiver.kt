package ir.code.challenge.utiles

import androidx.lifecycle.*

class LifeCycleAwarePushReceiver constructor(
    lifecycleOwner: LifecycleOwner, val onDestroyListener: () -> Unit) : DefaultLifecycleObserver {
    init {
        val lifecycle = lifecycleOwner.lifecycle
        if (lifecycle.currentState != Lifecycle.State.DESTROYED) {
            lifecycle.addObserver(this)
        }
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        onDestroyListener.invoke()
    }
}