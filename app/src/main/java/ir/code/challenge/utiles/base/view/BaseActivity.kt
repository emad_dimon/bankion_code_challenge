package ir.code.challenge.utiles.base.view

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import ir.code.challenge.BuildConfig

abstract class BaseActivity<T : ViewBinding> : AppCompatActivity() {

    protected val binding by lazy {
        bind()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        changeActivityDirection()


        initView()
    }

    private fun rtlActivity(): Boolean = BuildConfig.RTL

    private fun changeActivityDirection() {
        if (rtlActivity())
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
    }

    override fun attachBaseContext(newBase: Context?) {
        if (newBase != null)
            super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    protected abstract fun bind(): T

    protected abstract fun initView();
}