package ir.code.challenge.utiles

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.converter.gson.GsonConverterFactory

class GCFactory {
    private lateinit var gson: Gson;

    companion object {
        private var instance: GCFactory? = null

        fun getInstance(): GCFactory {
            return instance ?: synchronized(this) {
                instance ?: GCFactory().also {
                    instance = it
                    it.gson = GsonBuilder().create()
                }
            }
        }
    }

    fun gsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }
}