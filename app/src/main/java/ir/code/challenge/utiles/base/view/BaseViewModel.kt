package ir.code.challenge.utiles.base.view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel: ViewModel() {
    private var _errorLiveData: MutableLiveData<String> = MutableLiveData()

    var errorLiveData: MutableLiveData<String>
    get() {
        return _errorLiveData
    }
    set(value) {
        _errorLiveData = value
    }
}