package ir.code.challenge.utiles.ax


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.widget.ImageView
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import kotlin.math.ceil

/**
Created by Emad Mahouti on 7/8/21.
 */

class Ax(private val builder: Builder) {
    private val context: Context? = null

    companion object {
        fun load(url: String?): Builder {
            return Builder(url)
        }


        private fun getDimenSizedImageUrl(
            context: Context,
            url: String,
            @DimenRes dimenId: Int
        ): String? {
            return getSizedImageUrl(
                url,
                context.resources.getDimensionPixelSize(dimenId)
            )
        }

        private fun getSizedImageUrl(url: String, size: Int): String? {
            var size = size
            size = ceil((size / 50.0f).toDouble()).toInt() * 50
            return url.replace("%s", size.toString())
        }

        @JvmStatic
        fun load(@DrawableRes uri: Int): Builder {
            return Builder(uri)
        }

    }

    fun show() {
        try {
            if (builder.view.context == null || builder.view.context is Activity && (builder.view.context as Activity).isFinishing)
                return
            val request: RequestBuilder<Drawable>

            if (builder.isLoadResource()) {
                request = Glide.with(builder.view.context).load(builder.uriRes)
            } else {
                if (!TextUtils.isEmpty(builder.uriString) && builder.uriString != null) {
                    if (builder.size > 0) {
                        builder.uriString = builder.uriString?.let {
                            getSizedImageUrl(
                                it,
                                builder.size
                            )
                        }
                    } else if (builder.dimen != 0) {
                        builder.uriString =
                            getDimenSizedImageUrl(
                                builder.view.context,
                                builder.uriString!!,
                                builder.dimen
                            )
                    }
                }
                request = Glide.with(builder.view.context).load(builder.uriString)
            }
            applyRequestOption(request)
        } catch (e: Exception) {

        }

    }


    @SuppressLint("CheckResult")
    private fun applyRequestOption(request: RequestBuilder<Drawable>) {
        val context = builder.view.context
        if (builder.loading != 0) {
            request.placeholder(ContextCompat.getDrawable(context, builder.loading))
        } else if (builder.placeholder != 0) {
            request.placeholder(ContextCompat.getDrawable(context, builder.placeholder))
        }
        if (builder.error != 0) {
            request.error(ContextCompat.getDrawable(context, builder.error))
        }

        if (builder.withCrossFade) {
            val factory = DrawableCrossFadeFactory.Builder()
                .setCrossFadeEnabled(true)
                .build()
            request.transition(DrawableTransitionOptions.withCrossFade(factory))
        }
        request.into(builder.view)
    }

    class AnimationType {

        constructor(duration: Long) {
            this.duration = duration
            animType = AnimType.Rotating
        }

        var duration: Long = 0
        var animType: AnimType? = null

        enum class AnimType {
            Rotating
        }
    }

    class Builder {
        constructor(uri: Int) {
            uriRes = uri
        }

        constructor(uri: String?) {
            uriString = uri
        }

        lateinit var view: ImageView
        var uriRes = 0
        var placeholder = 0
        var uriString: String? = null
        var loading = 0
        var error = 0
        var animationType: AnimationType? = null
        var radius = 0
        var size = 0
        var dimen = 0
        var colorFilter: Int = -1
        var withCrossFade = true


        fun imageView(view: ImageView) = apply { this.view = view }
        fun uriRes(uri: Int) = apply { this.uriRes = uri }
        fun placeholder(placeholder: Int) = apply { this.placeholder = placeholder }
        fun uriString(uriString: String) = apply { this.uriString = uriString }
        fun loading(loading: Int) = apply { this.loading = loading }
        fun error(error: Int) = apply { this.error = error }

        fun radius(radius: Int) = apply { this.radius = radius }
        fun animationType(animationType: AnimationType) = apply { this.animationType = animationType }

        fun size(size: Int) = apply { this.size = size }
        fun dimen(dimen: Int) = apply { this.dimen = dimen }
        fun withCrossFade(crossFade: Boolean) = apply { this.withCrossFade = crossFade }

        fun isLoadResource(): Boolean {
            return uriRes != 0
        }

        fun into(view: ImageView?) {
            this.view = view!!
            Ax(this).show()
        }
    }

}