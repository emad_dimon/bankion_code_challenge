package ir.code.challenge.utiles.base

import com.google.gson.annotations.SerializedName

data class BaseModel<T>(
    val page: Int,
    val results: ArrayList<T>,

    @SerializedName("total_pages")
    val totalPages: Int,

    @SerializedName("total_results")
    val totalResults: Int
)