package ir.code.challenge.utiles

import android.content.Context
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import ir.code.challenge.data.network.AppClient
import ir.code.challenge.data.network.OkHttpClient
import ir.code.challenge.data.network.interceptors.CacheInterceptor

object InjectorUtils {
    fun appClient(context: Context): AppClient {
        return AppClient.getInstance(OkHttpClient.getInstance(context, CacheInterceptor()), GCFactory.getInstance())
    }

    val viewPumpBuilder: ViewPump by lazy {
        ViewPump.builder()
            .addInterceptor(
                CalligraphyInterceptor(
                    CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/fnum/IRANYekanMobileRegular.ttf")
//                        .setFontAttrId(R.attr.fontPath)
                        .build())
            )
            .build()
    }
}