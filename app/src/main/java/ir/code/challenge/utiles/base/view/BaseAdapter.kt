package ir.code.challenge.utiles.base.view

import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T, VH : BaseViewHolder<T>> : RecyclerView.Adapter<VH>() {

    private var baseAdapterItems: MutableList<T?> = ArrayList()
    var loading: Boolean = false
    var recyclerView: RecyclerView? = null
    var page: Int = 1
    var totalPages: Int? = null

    var items: List<T?>?
        get() {
            return baseAdapterItems
        }
        set(value) {
            if (value != null) {
                baseAdapterItems.addAll(value)
                notifyItemRangeInserted(baseAdapterItems.size, value.size)
                ++page
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return inflate(parent, viewType)
    }

    override fun getItemViewType(position: Int): Int = if(baseAdapterItems[position] == null) 0 else 1

    override fun getItemCount(): Int = baseAdapterItems.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = baseAdapterItems[holder.adapterPosition]
        if (item != null)
            (holder as BaseViewHolder<T>).bind(item)
    }

    abstract fun inflate(parent: ViewGroup, viewType: Int): VH

    inline fun touchBottom(recyclerView: RecyclerView, crossinline listener: (Int, Int) -> Unit) {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
//                val layoutManager =  recyclerView.layoutManager  as GridLayoutManager
//                if (loading) return
//
//                val visibleItemCount: Int = layoutManager.childCount
//                val totalItemCount: Int = layoutManager.itemCount
//                val pastVisibleItems: Int = layoutManager.findFirstVisibleItemPosition()
//                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
//                    listener.invoke(page - 1, page)
//                }
                if (!recyclerView.canScrollVertically(1) && !loading) {
                    listener.invoke(page - 1, page)
                }
            }
        })
    }

    fun loading(value: Boolean = true) {
        loading = value
        if (value) {
            if(baseAdapterItems[itemCount - 1] != null)
                addItem(null)
        } else {
           if(itemCount > 0 && baseAdapterItems[itemCount - 1]  == null)
               removeItem(itemCount -1)

        }
    }

    fun removeItem(position: Int) {
        baseAdapterItems.removeAt(position)
        notifyItemRemoved(position)
    }

    fun addItem(item: T?, position: Int = -1) {
        if (position == -1) {
            baseAdapterItems.add(item)
            notifyItemInserted(itemCount)
        } else {
            baseAdapterItems.add(position, item)
            notifyItemInserted(position)
        }
    }
}