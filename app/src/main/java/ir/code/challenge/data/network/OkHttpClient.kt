package ir.code.challenge.data.network

import android.content.Context
import ir.code.challenge.data.network.interceptors.CacheInterceptor
import ir.code.challenge.data.network.interceptors.HeaderInterceptor
import okhttp3.Cache
import java.io.File
import java.util.concurrent.TimeUnit

class OkHttpClient(context: Context, private var cacheInterceptor: CacheInterceptor) {
    companion object {
        const val TIMEOUT_SECONDS = 20L

        private var instance: OkHttpClient? = null

        fun getInstance(context: Context, cacheInterceptor: CacheInterceptor): OkHttpClient {
            return instance ?: synchronized(this) {
                instance ?: OkHttpClient(context, cacheInterceptor).also {
                    instance = it
                }
            }
        }
    }

    private fun cache(context: Context): Cache {
        val maxSize = 10 * 1024 * 1024L
        val cacheDir = File(context.cacheDir, "response")

        return Cache(cacheDir, maxSize)
    }

    val build: okhttp3.OkHttpClient by lazy {
        val okHttp = okhttp3.OkHttpClient.Builder()
        okHttp.connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .addInterceptor(HeaderInterceptor())
            .addNetworkInterceptor(cacheInterceptor)
            .cache(cache(context))

        okHttp.build()
    }
}