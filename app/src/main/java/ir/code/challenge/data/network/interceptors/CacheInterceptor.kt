package ir.code.challenge.data.network.interceptors

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class CacheInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()

        val maxAge = 60 // read from cache for 1 minute

        val newRequest =  request.newBuilder()
            .header("Cache-Control", "public, max-age=$maxAge")
            .build()
        
        return chain.proceed(newRequest)
    }
}