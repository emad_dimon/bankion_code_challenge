package ir.code.challenge.data.network

import ir.code.challenge.BuildConfig
import ir.code.challenge.utiles.GCFactory
import retrofit2.Retrofit

class AppClient(okHttpClient: OkHttpClient, gson: GCFactory) {

    var client: okhttp3.OkHttpClient? = null

    companion object {
        private var instance: AppClient? = null

        fun getInstance(okHttpClient: OkHttpClient, gson: GCFactory): AppClient {
            instance ?: synchronized(this) {
                instance ?: AppClient(okHttpClient, gson).also {
                    instance = it
                }
            }

            return instance!!
        }
    }

    private val getClient by lazy {
        client = okHttpClient.build

        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(client!!)
            .addConverterFactory(gson.gsonConverterFactory())
            .build()
    }

    fun <T> getService(service: Class<T>): T {
        return getClient.create(service)
    }
}