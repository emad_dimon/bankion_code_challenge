package ir.code.challenge.data.network.model

data class Company (val name: String)