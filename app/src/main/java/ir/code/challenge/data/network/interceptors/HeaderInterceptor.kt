package ir.code.challenge.data.network.interceptors

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()

        val newRequest: Request = request.newBuilder()
            .addHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4NzUwOGMwZWY5OWVmYThmZDUwMTkzNGYxM2ZkNDgzOCIsInN1YiI6IjYyNTgwNzM5YWZlMjI0MDA5ZDgyZTdjZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.VKDe4yaxy9ooBrIjGWZlR0pOGp6CWkswggWiR6c2gSM")
            .addHeader("Content-Type", "application/json;charset=utf-8")
            .build()

        return chain.proceed(newRequest)
    }
}