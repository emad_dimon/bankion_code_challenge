package ir.code.challenge.data.network.services

import ir.code.challenge.data.network.model.Movie
import ir.code.challenge.utiles.base.BaseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {
    @GET("/3/movie/popular")
    fun popular(@Query("page") page: Int): Call<BaseModel<Movie>>

    @GET("/3/movie/{movie_id}")
    fun detail(@Path("movie_id") id: Int): Call<Movie>
}