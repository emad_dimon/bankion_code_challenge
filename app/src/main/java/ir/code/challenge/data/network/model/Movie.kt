package ir.code.challenge.data.network.model

import com.google.gson.annotations.SerializedName
import ir.code.challenge.data.network.model.Company

data class Movie(
    val id: Int,

    @SerializedName("backdrop_path")
    val backdrop: String,

    @SerializedName("poster_path")
    val poster: String,

    @SerializedName("release_date")
    val release: String,

    @SerializedName("vote_average")
    val vote: Float,

    @SerializedName("production_companies")
    val companies: List<Company>?,

    val title: String,

    val overview: String
)